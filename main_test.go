package main_test

import (
	"context"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	afero "github.com/tbhartman/afero-lite/full"
	"github.com/tbhartman/afero-lite/mem"
	dustat "gitlab.com/tbhartman/dustat"
	"gitlab.com/tbhartman/dustat/internal/version"
)

func TestMain(t *testing.T) {
	var out strings.Builder
	var fs = mem.NewMemMapFs()
	fs.Mkdir("a", 0700)
	afero.WriteFile(fs, "a/a.txt", []byte("hello, world\n"), 0700)
	afero.WriteFile(fs, "a/b.txt", []byte("this is longer by alot alot alot\n"), 0700)
	err := dustat.Execute(context.Background(), strings.NewReader(""), &out, &out, fs, "a")
	a := assert.New(t)
	a.NoError(err)
	a.Equal(
		version.Get().String()+`

number of files:       2
number of directories: 1
number of 0B files:    0

min     size:   13     B
average size:   23     B
max     size:   33     B
total   size:   46     B

Histogram of file sizes ("S") and counts ("|"):
      33     B
  >   30     B: ---------------------------------------$ (1;   33     B)
  >   27     B: $                                        (0;    0     B)
  >   25     B: $                                        (0;    0     B)
  >   23     B: $                                        (0;    0     B)
  >   21     B: $                                        (0;    0     B)
  >   19     B: $                                        (0;    0     B)
  >   17     B: $                                        (0;    0     B)
  >   16     B: $                                        (0;    0     B)
  >   14     B: $                                        (0;    0     B)
  >   13     B: ---------------S-----------------------| (1;   13     B)

top extensions: (case insensitive)
  by size:
    .txt (x2)   46     B
  by count:
    .txt (x2)   46     B

`+version.Get().Name+` complete in 0s
`,
		out.String(),
	)
}
