package version

import (
	"bytes"
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestVersionUnmarshal(t *testing.T) {
	a := assert.New(t)
	var v Version
	a.NoError(json.Unmarshal([]byte(`
{
  "buildDate": "2021-01-01 00:00:00"
}`), &v))

	a.EqualValues(2021, v.BuildDate.Year())

}

func TestVersionMarshal(t *testing.T) {
	a := assert.New(t)
	var v Version = Version{
		Name:   "test",
		Commit: "12345",
		Dirty:  false,
		Tag:    "v1",
		Branch: "master",
	}
	var b bytes.Buffer
	var m = json.NewEncoder(&b)
	m.SetIndent("", "  ")
	err := m.Encode(&v)
	a.NoError(err)
	a.EqualValues(`{
  "Name": "test",
  "Commit": "12345",
  "Dirty": false,
  "Tag": "v1",
  "Branch": "master",
  "BuildDate": "0001-01-01 00:00:00"
}
`, b.String())

}

// TestVersionsMarshal attempts to check that a Versions in []interface{}
// is marshalled correctly, because using (v *Version) Marshal instead of
// (v Version) Marshal was causing problems.  This doesn't actually test for
// that, so I'm not sure why RPC with this was breaking...
func TestVersionsMarshal(t *testing.T) {
	a := assert.New(t)
	var vv []interface{}
	f := func(i ...interface{}) []interface{} {
		return i
	}
	vv = f(&Version{
		Name:   "test",
		Commit: "12345",
		Dirty:  false,
		Tag:    "v1",
		Branch: "master",
	})
	b, err := json.Marshal(vv)
	a.NoError(err)
	a.EqualValues(`[{"Name":"test","Commit":"12345","Dirty":false,"Tag":"v1","Branch":"master","BuildDate":"0001-01-01 00:00:00"}]`, string(b))
}
