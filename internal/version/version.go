package version

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"time"
)

//go:generate go run -tags version ./generate/main.go -o versioned.go

type Version struct {
	Name      string
	BuildDate time.Time
	Commit    string
	Dirty     bool
	Tag       string
	Branch    string
}

var thisVersion Version

const versionDateFormatStr string = "2006-01-02 15:04:05"

// MarshalJSON via http://choly.ca/post/go-json-marshalling/
func (v Version) MarshalJSON() ([]byte, error) {
	type Alias Version
	return json.Marshal(&struct {
		*Alias
		BuildDate string
	}{
		Alias:     (*Alias)(&v),
		BuildDate: v.BuildDate.In(time.UTC).Format(versionDateFormatStr),
	})
}

func (v *Version) UnmarshalJSON(b []byte) error {
	type Alias Version
	var d = &struct {
		*Alias
		BuildDate string
	}{
		Alias: (*Alias)(v),
	}
	err := json.Unmarshal(b, &d)
	if err != nil {
		return err
	}

	date, err := time.Parse(versionDateFormatStr, d.BuildDate)
	if err != nil {
		return err
	}
	v.BuildDate = date
	return nil
}

func (v Version) String() string {
	var versionString string
	switch {
	case v.Tag != "":
		versionString = v.Tag
	case v.Commit != "":
		versionString = v.Commit
	case !v.BuildDate.IsZero():
		versionString = v.BuildDate.String()
	default:
		versionString = "unknown"
	}
	return fmt.Sprintf(
		"%s version %s %s/%s",
		v.Name,
		versionString,
		runtime.GOOS,
		runtime.GOARCH,
	)
}

func Get() Version {
	ret := thisVersion
	ret.Name = filepath.Base(os.Args[0])
	ret.Name = ret.Name[:len(ret.Name)-len(filepath.Ext(ret.Name))]
	return ret
}
