//go:build version
// +build version

package main

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/tbhartman/dustat/internal/version"
)

func main() {
	if len(os.Args) != 3 || os.Args[1] != "-o" {
		fmt.Println("Invalid arguments")
		fmt.Println("expected '-o <filename>'")
	}
	f, err := os.Create(os.Args[2])
	if err != nil {
		log.Fatalln(err)
	}
	defer f.Close()

	err = version.Main(f)
	if err != nil {
		log.Fatal(err)
	}
}
