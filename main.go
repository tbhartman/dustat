package main

import (
	"context"
	"fmt"
	"io"
	"os"
	"os/signal"
	"syscall"

	"github.com/spf13/cobra"
	"github.com/tbhartman/afero-lite"
	verMod "gitlab.com/tbhartman/dustat/internal/version"
)

var errCobra = fmt.Errorf("error from cobra")

var version = verMod.Get()

type GlobalConfig struct {
	CobraRan    bool
	CobraHelp   bool
	CommandName string
	CPUProfile  string
}

type runner = func(afero.Fs) error

type writeListener struct {
	io.Writer
	Written bool
}

func (w *writeListener) Write(b []byte) (int, error) {
	w.Written = true
	return w.Writer.Write(b)
}

// if RunError is not nil, prog should exit after calling runCobra
func runCobra(
	ctx context.Context,
	conf *GlobalConfig,
	stdin io.Reader,
	stdout io.Writer,
	stderr io.Writer,
	args []string,
) (runner, error) {
	var runner runner
	var (
		dustatConf dustatOptions
	)
	dustatConf.Stdin = stdin
	rootCmd := &cobra.Command{
		Use:     version.Name,
		Version: version.String(),
		Args:    cobra.ArbitraryArgs,
		PersistentPreRun: func(cmd *cobra.Command, args []string) {
			conf.CommandName = version.Name + " " + cmd.CalledAs()
		},
		Run: func(cmd *cobra.Command, args []string) {
			runner = func(f afero.Fs) error {
				return dustat(ctx, stdout, f, args, dustatConf)
			}
		},
		PersistentPostRun: func(cmd *cobra.Command, args []string) {
			conf.CobraRan = true
		},
	}

	rootCmd.SetVersionTemplate("{{.Version}}\n")
	rootCmd.PersistentFlags().IntVarP(&dustatConf.ExtCount, "count", "n", 10, "number of extensions in listing")

	sErr := &writeListener{Writer: stderr}
	sOut := &writeListener{Writer: stdout}

	rootCmd.SetArgs(args)
	rootCmd.SetErr(sErr)
	rootCmd.SetOut(sOut)
	rootCmd.SetIn(stdin)

	err := rootCmd.Execute()

	// report if cobra ran into an error
	if err != nil {
		return nil, err
	}

	// if cobra did not run into an error, but CobraRan is false, it must
	// have handled all it needed to
	if !conf.CobraRan {
		return nil, nil
	}
	// if cobra wrote to stderr or stdout, i should probably exit
	if sErr.Written || sOut.Written {
		return nil, errCobra
	}

	return runner, nil
}

type dustatOptions struct {
	Stdin    io.Reader
	ExtCount int
}

func Execute(
	ctx context.Context,
	stdin io.Reader,
	stdout io.Writer,
	stderr io.Writer,
	fs afero.Fs,
	args ...string,
) error {
	var conf GlobalConfig

	r, err := runCobra(ctx, &conf, stdin, stdout, stderr, args)

	if err != nil {
		return err
	}
	if r == nil {
		return nil
	}

	return r(fs)
}

func main() {
	ctxTerm, stopTerm := signal.NotifyContext(context.Background(), syscall.SIGTERM)
	defer stopTerm()
	ctxInterrupt, stopInterrupt := signal.NotifyContext(ctxTerm, os.Interrupt)
	defer stopInterrupt()

	var err error
	err = Execute(ctxInterrupt, os.Stdin, os.Stdout, os.Stderr, afero.NewOsFs(), os.Args[1:]...)
	if err != nil {
		if err != errCobra {
			fmt.Printf("ERROR: %s\n", err.Error())
		}
		os.Exit(1)
	}
}
