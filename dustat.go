package main

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	afero "github.com/tbhartman/afero-lite/full"
	"github.com/tbhartman/go-histogram"
	"gitlab.com/tbhartman/dustat/internal/humanize"
)

func isTerminal(w io.Writer) bool {
	f, ok := w.(*os.File)
	if !ok || f == nil {
		return false
	}
	s, _ := f.Stat()
	return s != nil && (s.Mode()&os.ModeCharDevice) != 0
}

func applyToSlice[T any](sp *[]T, f func(T) ([]T, error)) error {
	s := *sp
	var sLen = len(s)
	for i := 0; i < sLen; i++ {
		element := s[0]
		copy(s, s[1:])
		newElements, err := f(element)
		if err != nil {
			return err
		}
		s = append(s[:len(s)-1], newElements...)
	}
	sp = &s
	return nil
}

type fileList struct {
	Directories  int64
	ExtensionMap map[string][]int64
}

type twirler struct {
	state  int
	String string
}

func (t *twirler) Next() string {
	t.state++
	if t.state >= len(t.String) {
		t.state = 0
	}
	return string(t.String[t.state])
}

func findFiles(ctx context.Context, w io.Writer, f afero.Fs, pathSlice []string) (fileList, error) {
	var fileCount int64
	var fileSum uint64

	termCtx, termCloser := context.WithCancel(ctx)
	defer termCloser()

	var termWg sync.WaitGroup

	if isTerminal(w) {
		termWg.Add(1)
		var timer = time.NewTicker(time.Millisecond * 500)
		var twirl = twirler{
			String: "|/-\\",
		}
		go func() {
			defer termWg.Done()
			var maxLineLength int = 12
			fmt.Fprint(w, "searching...\r")
			for {
				select {
				case <-termCtx.Done():
					timer.Stop()
					fmt.Fprint(w, strings.Repeat(" ", maxLineLength)+"\r")
					return
				case <-timer.C:
					line := fmt.Sprintf(
						"%s found %d files (%s)",
						twirl.Next(),
						atomic.LoadInt64(&fileCount),
						humanize.Bytes(atomic.LoadUint64(&fileSum)),
					)
					for len(line) < maxLineLength {
						line += " "
					}
					maxLineLength = len(line)
					fmt.Fprint(w, line+"\r")
				}
			}
		}()
	}

	listing := fileList{
		ExtensionMap: make(map[string][]int64),
	}

	// TODO: check that i'm not Walk'ing over duplicate paths

	for i := 0; i < len(pathSlice); i++ {
		path := pathSlice[i]
		files, err := afero.ReadDir(f, path)
		if err != nil {
			s, err := f.Stat(path)
			if err != nil {
				continue
			}
			files = append(files, s)
		}
		select {
		case <-ctx.Done():
			termWg.Wait()
			fmt.Fprintln(w, "user cancelled process...")
			return listing, ctx.Err()
		default:
		}
		listing.Directories++
		for _, f := range files {
			fpath := path + "/" + f.Name()
			if f.IsDir() {
				pathSlice = append(pathSlice, fpath)
			} else {
				ext := strings.ToLower(filepath.Ext(fpath))
				size := f.Size()
				listing.ExtensionMap[ext] = append(listing.ExtensionMap[ext], size)
				atomic.AddInt64(&fileCount, 1)
				atomic.AddUint64(&fileSum, uint64(size))
			}
		}
	}
	termCloser()
	termWg.Wait()
	return listing, nil
}

type statsResult struct {
	Zeros   int
	Min     uint64
	Max     uint64
	Average int64
	Count   int64
	Sum     uint64
}

func stats(s []int64) statsResult {
	var ret statsResult
	for _, v := range s {
		ret.Count++
		uv := uint64(v)
		ret.Sum += uv
		if v == 0 {
			ret.Zeros++
		}
		if ret.Count == 1 {
			ret.Min = uv
			ret.Max = uv
		} else if ret.Min > uv {
			ret.Min = uv
		} else if ret.Max < uv {
			ret.Max = uv
		}
	}
	if ret.Count > 0 {
		ret.Average = int64(float64(ret.Sum) / float64(ret.Count))
	}
	return ret
}

type extResultElement struct {
	Extension string
	Count     int
	Size      uint64
}

func dustat(ctx context.Context, w io.Writer, fs afero.Fs, paths []string, opt dustatOptions) error {
	start := time.Now()
	fmt.Fprintln(w, version.String())

	var includeStdin bool
	for i, p := range paths {
		if p == "-" {
			if i != len(paths)-1 {
				return fmt.Errorf("single dash must be last in paths")
			}
			includeStdin = true
			paths = paths[:len(paths)-1]
		}
	}
	if includeStdin {
		s := bufio.NewScanner(opt.Stdin)
		for s.Scan() {
			if len(s.Text()) > 0 {
				paths = append(paths, s.Text())
			}
		}
	}

	// glob paths
	err := applyToSlice(&paths, func(p string) ([]string, error) {
		return afero.Glob(fs, p)
	})
	if err != nil {
		return err
	}
	sort.Strings(paths)
	// make unique
	for i := 0; i < len(paths)-1; i++ {
		if paths[i] == paths[i+1] {
			copy(paths[i:], paths[i+1:])
			paths = paths[:len(paths)-1]
		}
	}

	results, err := findFiles(ctx, w, fs, paths)

	if err != nil {
		return err
	}

	var allFiles []int64
	for _, e := range results.ExtensionMap {
		allFiles = append(allFiles, e...)
	}
	hist := histogram.CreateLog(allFiles, 10)
	s := stats(allFiles)

	length := len(strconv.Itoa(len(allFiles)))

	fmt.Fprintf(w, "\nnumber of files:       %*d\n", length, len(allFiles))
	fmt.Fprintf(w, "number of directories: %*d\n", length, results.Directories)
	fmt.Fprintf(w, "number of 0B files:    %*d\n\n", length, s.Zeros)
	fmt.Fprintf(w, "min     size: %s\n", humanize.BytesPad(s.Min))
	fmt.Fprintf(w, "average size: %s\n", humanize.BytesPad(s.Average))
	fmt.Fprintf(w, "max     size: %s\n", humanize.BytesPad(s.Max))
	fmt.Fprintf(w, "total   size: %s\n\n", humanize.BytesPad(s.Sum))
	fmt.Fprintln(w, `Histogram of file sizes ("S") and counts ("|"):`)
	histogram.Print(w, hist, histogram.GetBytesOptions[int64](40, true))

	// arrange results by extension, count, sum
	var extResult []extResultElement
	for ext, value := range results.ExtensionMap {
		extResult = append(extResult, extResultElement{
			Extension: ext,
			Count:     len(value),
			Size:      stats(value).Sum,
		})
	}

	// limit ExtCount to number of results
	if opt.ExtCount > len(extResult) {
		opt.ExtCount = len(extResult)
	}

	// determine top extensions by size and file count
	var bySize_byCount = make([]extResultElement, opt.ExtCount*2)
	sort.Slice(extResult, func(i, j int) bool { return extResult[i].Size > extResult[j].Size })
	copy(bySize_byCount[:opt.ExtCount], extResult)
	sort.Slice(extResult, func(i, j int) bool { return extResult[i].Count > extResult[j].Count })
	copy(bySize_byCount[opt.ExtCount:], extResult)

	// determine strings lengths of ext, count
	var maxExtLen int
	var maxExtCount int
	for _, r := range bySize_byCount {
		if len(r.Extension) > maxExtLen {
			maxExtLen = len(r.Extension)
		}
		if r.Count > maxExtCount {
			maxExtCount = r.Count
		}
	}
	maxExtCount = len(strconv.Itoa(maxExtCount))

	// print extensions
	p := func(s []extResultElement) {
		for _, s := range s {
			fmt.Fprintf(w, "    %*s (x%*d) %s\n", maxExtLen, s.Extension, maxExtCount, s.Count, humanize.BytesPad(s.Size))
		}
	}
	fmt.Fprintln(w, "\ntop extensions: (case insensitive)")
	fmt.Fprintln(w, "  by size:")
	p(bySize_byCount[:opt.ExtCount])
	fmt.Fprintln(w, "  by count:")
	p(bySize_byCount[opt.ExtCount:])

	dur := time.Since(start)
	dur = dur.Truncate(time.Millisecond * 100)

	fmt.Fprintf(w, "\n%s complete in %s\n", version.Name, dur)

	return nil
}
